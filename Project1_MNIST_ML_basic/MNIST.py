
import os
import numpy as np
import idx2numpy
import matplotlib.pyplot as plt

from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import array_to_img

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

train_images_filepath = 'MNIST/train-images-idx3-ubyte'
train_labels_filepath = 'MNIST/train-labels-idx1-ubyte'
test_images_filepath = 'MNIST/t10k-images-idx3-ubyte'
test_labels_filepath = 'MNIST/t10k-labels-idx1-ubyte'

train_imagearray = idx2numpy.convert_from_file(train_images_filepath)
train_labelarray = idx2numpy.convert_from_file(train_labels_filepath)

validation_imagearray = train_imagearray[55000:60000]
validation_labelarray = train_labelarray[55000:60000]
train_imagearray = train_imagearray[0:55000]
train_labelarray = train_labelarray[0:55000]

test_imagearray = idx2numpy.convert_from_file(test_images_filepath)
test_labelarray = idx2numpy.convert_from_file(test_labels_filepath)

print(train_imagearray.shape)
print(train_labelarray.shape)
print(validation_imagearray.shape)
print(validation_labelarray.shape)
print(test_imagearray.shape)
print(test_labelarray.shape)

### Save and show the train dataset
# save_dir = 'MNIST/raw/'

# # Save original images in folder "raw"
# if os.path.exists(save_dir) is False:
#     os.makedirs(save_dir)

# # Save first 20 images
# for i in range(20):
#     image_array = train_imagearray[i, :]
#     image_array = image_array.reshape(28, 28, 1)
#     filename = save_dir + 'mnist_train_%d.jpg' % i
#     img = array_to_img(image_array)
#     img.save(filename)

### Softmax regression implenmentation
x = tf.placeholder(tf.float32, [None, 784]) # placeholder
W = tf.Variable(tf.zeros([784, 10]))        # weight
b = tf.Variable(tf.zeros([10]))             # bias

y = tf.nn.softmax(tf.matmul(x, W) + b)      # output form module
y_ = tf.placeholder(tf.float32, [None, 10]) # real image label

cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y)))

train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)    # learning rate: 0.01

### Before optimization, we must create session 
session = tf.InteractiveSession()
tf.global_variables_initializer().run()


for i in range(1000):
    startIndex = i * 55
    endIndex = startIndex + 100
    batch_xs = train_imagearray[startIndex:endIndex]
    batch_ys = train_labelarray[startindex:endIndex]
    session.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})


correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

print(session.run(accuracy, feed_dict={x: test_imagearray, y_:test_labelarray}))

