﻿# coding:utf-8

import cifar10
import tensorflow as tf

FLAGS = tf.app.flags.FLAGS
FLAGS.data_dir = 'CIFAR-10/'

# if there is no dataset exsited, then download it
cifar10.maybe_download_and_extract()
