﻿#coding: utf-8
import cifar10_input
import tensorflow as tf
import os
import scipy.misc


def inputs_origin(data_dir):
  filenames = [os.path.join(data_dir, 'data_batch_%d.bin' % i)
               for i in xrange(1, 6)] # 'xrange' has been removed from the python3 library

  for f in filenames:
    if not tf.gfile.Exists(f):
      raise ValueError('Failed to find file: ' + f)

  filename_queue = tf.train.string_input_producer(filenames)

  # Return value is the Tensor of the images with attribute 'uint8image'
  read_input = cifar10_input.read_cifar10(filename_queue)
  # Convert image into real number format
  reshaped_image = tf.cast(read_input.uint8image, tf.float32)

  return reshaped_image

if __name__ == '__main__':
  with tf.Session() as sess:
    reshaped_image = inputs_origin('CIFAR-10/cifar-10-batches-bin')
    # start_queue_runners is neccessary for us to train
    threads = tf.train.start_queue_runners(sess=sess)
    sess.run(tf.global_variables_initializer())

    if not os.path.exists('CIFAR-10/raw/'):
      os.makedirs('CIFAR-10/raw/')

    for i in range(30):
      image_array = sess.run(reshaped_image)
      scipy.misc.toimage(image_array).save('CIFAR-10/raw/%d.jpg' % i)
