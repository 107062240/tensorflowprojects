import tensorflow as tf

with tf.Session() as sess:
    filename = ['A.jpg', 'B.jpg', 'C.jpg']
    # string_input_producer will create a filenames' storage stack
    filename_queue = tf.train.string_input_producer(filename, shuffle=False, num_epochs=5)
    # reader fetches the data from the stack
    reader = tf.WholeFileReader()
    key, value = reader.read(filename_queue)
    tf.local_variables_initializer().run()
    threads = tf.train.start_queue_runners(sess=sess)

    i = 0

    while True:
        i += 1
        image_data = sess.run(value)
        with open('read/test_%d.jpg' % 1, 'wb') as f:
            f.write(image_data)