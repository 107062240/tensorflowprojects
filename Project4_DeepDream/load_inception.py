﻿
from __future__ import print_function
import numpy as np
import tensorflow as tf

# creat graph and Session
graph = tf.Graph()
sess = tf.InteractiveSession(graph=graph)

model_fn = 'tensorflow_inception_graph.pb'
with tf.gfile.FastGFile(model_fn, 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())

# Define t_input as our input
t_input = tf.placeholder(np.float32, name='input')
imagenet_mean = 117.0

# expand_dims: from [height, width, channel] to [1, height, width, channel]
t_preprocessed = tf.expand_dims(t_input - imagenet_mean, 0)
tf.import_graph_def(graph_def, {'input': t_preprocessed})

# Find out all Convolutional layers
layers = [op.name for op in graph.get_operations() if op.type == 'Conv2D' and 'import/' in op.name]

print('Number of layers', len(layers))

# Specificallyt output the shape of mixed4d_3x3_bottleneck_pre_relu
name = 'mixed4d_3x3_bottleneck_pre_relu'
print('shape of %s: %s' % (name, str(graph.get_tensor_by_name('import/' + name + ':0').get_shape())))
